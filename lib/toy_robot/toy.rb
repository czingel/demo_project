module Cli
  class Toy
    DIRECTIONS = %w[north east south west].freeze
    REPORT_ERROR = "Robot is not on the table".freeze
    BLANK_MSG = "".freeze

    public
    def initialize()
      @history = []
      @position = nil
    end

    attr_reader :history

    # Public: places the toy robot onto the table
    # @param [Integer] x
    # set the x co-ordinate
    #
    # @param [Integer] y
    # set the y co-ordinate
    #
    # @param [string] direction
    # set the facing direction i.e. North, South, East and West
    def place(x, y, direction)
      new_position = { x: x.to_i, y: y.to_i, direction: direction }
      if valid_position(new_position)
        @position = new_position
        @history = [] << "place #{x}, #{y},#{direction}"
      end
      return BLANK_MSG
    end

    # Public: turns the robot left
    def left
      return if @position.nil?
      @position[:direction] = change_direction(@position[:direction], 'left')
      @history  << "left"
      return BLANK_MSG
    end

    # Public: turns the robot right
    def right
      return if @position.nil?
      @position[:direction] = change_direction(@position[:direction], 'right')
      @history  << "right"
      return BLANK_MSG
    end

    # Public: reports the location of the robot
    # @return [string] with co-ordinates and  robot direction
    def report
      if @position
        @history  << "report"
        "#{@position[:x]},#{@position[:y]},#{@position[:direction].upcase}"
      else
        REPORT_ERROR
      end
    end

    # Public: move the robot forward one square
    def move
      return if  @position.nil?

      new_position =@position.dup

      case new_position[:direction]
      when 'north'
        new_position[:y] += 1
      when 'south'
        new_position[:y] -= 1
      when 'east'
        new_position[:x] += 1
      when 'west'
        new_position[:x] -= 1
      else
        raise 'Unknown Direction'
      end
      if valid_position(new_position)
        @position = new_position
        @history  << "move"
      end
      return BLANK_MSG
    end

    private

    # Private: changes the direction the robot is facing
    # @params [string] direction
    # the initial direction the robot is facing
    #
    # @params[string move
    # whether we are moving left or right
    def change_direction(direction, move)
      index_i = DIRECTIONS.index(direction)
      move_i = move == 'left' ? -1 : 1
      DIRECTIONS[(index_i + move_i) % DIRECTIONS.count]
    end

    # Private: checks whether the move was valid for the robot 
    #
    # @params [hash] location position 
    def valid_position(new_position)
      if DIRECTIONS.include?(new_position[:direction]) && new_position[:x] >= 0 && new_position[:x] <= 5 && new_position[:y] >= 0 && new_position[:y] <= 5
        true
      else
        false
      end
    end
  end
end
