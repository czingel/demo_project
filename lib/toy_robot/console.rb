require 'highline'
module Cli
  class Console
    public

    def initialize
      @toy = Toy.new
      @available_commands = @toy.public_methods(false)
      @additional_commands = %w[help exit]
      @cli = HighLine.new
      yield(self) if block_given? # additional configuration
    end

    attr_accessor :toy
    attr_accessor :cli
    attr_accessor :available_commands
    attr_accessor :additional_commands

    # public: the toy robot console
    # Loops through issued commands. The command 'exit' breaks the loop.
    def start
      history = []
      horizontal_line
      @cli.say("Welcome to your Toy Robot")
      @cli.say("Please Enter command or 'help' for assistence:")

      loop do
        begin
          answer = read_instructions
          @cli.say answer
          break if answer == 'exit'
        rescue => err
          puts  err
        end
      end
    end

    def horizontal_line
      @cli.say('*' * 100)
    end

    # public: Outputs the available commands
    def help
      horizontal_line
      @cli.say('Available Commands:')
      (@additional_commands + @available_commands).each do |command|
        @cli.say(command.to_s)
      end
      horizontal_line
    end

    # public: outputs 'Bye'
    # @return [string] exit which breaks the loop in method start
    def exit
      horizontal_line
      @cli.say('Bye now.')
      'exit'
    end

    private

    # private: reads the input instructions and proxies them to the  toy object
    #
    # @params [string] input_text
    # sets up the initial messaging
    def read_instructions(input_text = '')
      instruction = @cli.ask(input_text) do |h|
        h.whitespace = :strip_and_collapse
      end

      instruction.downcase!

      # Checks whether the command is a toy public method
      @additional_commands.each do |key|
        return send(key) if key.start_with?(instruction) || key == instruction
      end

      # Checks whether the command is a console method (ie bye and help)
      @available_commands.each do |key|
        return @toy.send(*parse_instruction(instruction)) if instruction.start_with?(key.to_s) || key == instruction
      end
      return "Unknown command: #{instruction}. Enter Help for list of commands"
    end

    def parse_instruction(str)
      str_match = str.match(/\A(?<command>\w*)(?<params>.*)/)
      [str_match[:command].to_sym].concat(str_match[:params].split(','))
    end
  end
end
