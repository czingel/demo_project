require 'spec_helper'

RSpec.describe Cli::Toy do
  context "with place set at zero, zero north" do
    before do
      @toy = Cli::Toy.new
      @toy.place(0,0,"north")
    end
    context '#move' do
      it 'should correctly move to corret square' do
        @toy.move
        expect(@toy.report).to eql("0,1,NORTH")
      end

      it 'should not fall off the table by moving to far ' do
        @toy.right
        10.times { @toy.move }
        expect(@toy.report).to eql("5,0,EAST")
      end

      it 'should correctly move to corret square' do
        @toy.move
        expect(@toy.report).to eql("0,1,NORTH")
        @toy.right
        @toy.move
        expect(@toy.report).to eql("1,1,EAST")
      end
    end

    context '#right' do
      it 'should correctly move to the next orientation' do
        @toy.right
        expect(@toy.report).to eql("0,0,EAST")
      end
    end

    context '#left' do
      it 'should correctly move to the next orientation' do
        expect(@toy.report).to eql("0,0,NORTH")
        @toy.left
        expect(@toy.report).to eql("0,0,WEST")
        @toy.left
        expect(@toy.report).to eql("0,0,SOUTH")
        2.times { @toy.left }
        expect(@toy.report).to eql("0,0,NORTH")
      end
    end
  end

  context "with place set at zero, zero north" do
    before do
      @toy = Cli::Toy.new
    end
    context '#place' do
      it 'should move to a valid position' do
        @toy.place(2, 2, 'west')
        expect(@toy.report).to eql("2,2,WEST")
      end

      it 'should ignore any invalid parameters' do
        @toy.place(2, 2, 'wwwwest')
        expect(@toy.report).to eql(::Cli::Toy::REPORT_ERROR)
      end

      it 'should ignore any invalid placements' do
        @toy.place(2, -2, 'west')
        expect(@toy.report).to eql(::Cli::Toy::REPORT_ERROR)
      end
    end
  end
end
