require 'spec_helper'

RSpec.describe Cli::Console do
  DEFAULT_POSITION = { x: 0, y: 0, direction: 'north' }.freeze

  before do
    @input    = StringIO.new
    @output   = StringIO.new
    @console = Cli::Console.new do |a|
      a.cli = HighLine.new(@input, @output)
    end
  end

  context 'Unhappy path' do
    it 'should ignore prior commands before place' do
      @input << "MOVE\nREPORT\nexit"
      @input.rewind
      @console.start
      expect(@output.string.split("\n").include?(::Cli::Toy::REPORT_ERROR)).to be true
    end

    it 'should ignore unknown commands' do
      @input << "JUMP OVER\nREPORT\nPLACE 5,5,NORTH\nREPORT\nexit"
      @input.rewind
      @console.start
      expect(@output.string.split("\n").include?(::Cli::Toy::REPORT_ERROR)).to be true
      expect(@output.string.split("\n").include?('5,5,NORTH')).to be true
    end
  end
  context 'Given examples' do

    it 'first example (move)' do
      # @input << "PLACE 0,0,NORTH\nexit"
      @input << "PLACE 0,0,NORTH\nMOVE\nREPORT\nexit"
      @input.rewind
      @console.start
      expect(@output.string.split("\n").include?('0,1,NORTH')).to be true
    end
    it 'second example (left)' do
      @input << "PLACE 0,0,NORTH\nLEFT\nREPORT\nexit"
      @input.rewind
      @console.start
      expect(@output.string.split("\n").include?('0,0,WEST')).to be true
    end
    it 'third example (left and move)' do
      @input << "PLACE 1,2,EAST\nMOVE\nMOVE\nLEFT\nMOVE\nREPORT\nexit"
      @input.rewind
      @console.start
      expect(@output.string.split("\n").include?('3,3,NORTH')).to be true
    end
  end
end
